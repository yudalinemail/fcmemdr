package DataProcess;

import java.util.Map;

/**
 * The OutcomeVariableTools is used to provide the information of categorical column
 *     for outcome variable.
 * */

public class OutcomeVariableTools 
{
	/**
	 * The heading of outcome variable.
	 * */
	
	private String m_heading = "";
	
	/**
	 * The names of items in outcome variable.
	 * */
	
	private int[] m_itemNames = null;
	
	/**
	 * The numbers of items in outcome variable.
	 * */
	
	private int[] m_cetagoryNumber = null;
	
	/**
	 * The NumberOfType is used to compute the total number in each type.
	 * Each total number aims to provide the numbers of types in each n-fold data. 
	 * */
	
	private int[] m_numberOfType = null;
	
	/**
	 * Construct OutcomeVariableTools which can hold the specified outcome column.
	 * 	
	 * @param	column	The outcome variable column.
	 * 
	 * @param	nfold	The number of fold in the experiment.
	 * */
	
	public OutcomeVariableTools( CategoryColumn column , String nfold )
	{
		Map< Integer , Integer > Category = column.getCategoryCount();
		m_heading = column.getHeading();
		m_itemNames = new int[ Category.keySet().size() ];
		m_cetagoryNumber = new int[ Category.keySet().size() ];
		m_numberOfType = new int[ Category.keySet().size() ];
		int index = 0;
		for( Map.Entry< Integer , Integer > entry:Category.entrySet() )
		{
			m_itemNames[ index ] = entry.getKey();
			m_cetagoryNumber[ index ] = entry.getValue();
			m_numberOfType[ index ] = m_cetagoryNumber[ index ]/( nfold.equals( "5-fold" ) ? 5 : 10 );
			index++;
		}
	}
	
	/**
	 * Return the heading of outcome variable.
	 * 
	 * @return	The heading of outcome variable.
	 * */
	
	public String getHeading()
	{
			return m_heading;
	}
	
	/**
	 * Return the names of items in outcome variable.
	 * 
	 * @return	The names of items in outcome variable.
	 * */
	
	public int[] getCategoryName()
	{
		return m_itemNames;
	}
	
	/**
	 * Return the numbers of items in outcome variable.
	 * 
	 * @return	The numbers of items in outcome variable.
	 * */
	
	public int[] getCategoryNumber()
	{
			return m_cetagoryNumber;
	}
	
	/**
	 * Return the total number in each type.
	 * 
	 * @return	The total number in each type.
	 * */
	
	public int[] getNumberOfType()
	{
			return m_numberOfType;
	}
	
	/**
	 * Return the specific name of item.
	 * 
	 * @param	index	The index of the specific item in column.
	 * 
	 * @return	The specific name of item.
	 * */
	
	public int getCategoryName( int index )
	{
			return m_itemNames[ index ];
	}
	
	/**
	 * Return the number of specific item.
	 * 
	 * @param	index	The index of specific item in column.
	 * 
	 * @return	The number of specific item.
	 * */
	
	public int getCategoryNumber( int index )
	{
			return m_cetagoryNumber[ index ];
	}
	
	
	/**
	 * Return the total number of specific type.
	 * 
	 * @param	name	The name of specific item in column.
	 * 
	 * @return	The total number of specific type.
	 * */
	
	public int getNumberOfType( int name )
	{
		for( int i=0 ; i < m_itemNames.length ; i++ )
		{
			if( m_itemNames[ i ] == name )
			{
				return m_numberOfType[ i ];
			}
		}
		return 0;
	}
}

/*
 * <p>
 * Copyright &copy; 2015-2016 National Kaohsiung University of Applied Sciences.
 * Author: Yu-Da Lin
 * </p>
 * <p>
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * </p>
 * <p>
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more
 * details.
 * </p>
 * <p>
 * You should have received a copy of the GNU General Public
 * License along with this program.
 * </p>
 */
