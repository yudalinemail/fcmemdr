package MulitifactorDimensionalityReduction;

import java.util.Map;
import java.util.Map.Entry;

import FitnessFunction.fitnessFunction;
import Pareto.ObjectiveSolution;
import Pareto.ParetoOptimalFrontSolutionsSet;

public class CalculateFuzzyCmeansEntropyPredictionError extends CalculatePredictionError{
	
	double m = 2;
	
	public void calculate(TestData testData, ParetoOptimalFrontSolutionsSet trainingDataCandidate, int seed)
	{
		
		for(TrainingData trainingData:trainingDataCandidate.getParetoOptimaTrainingData())
		{
			TestData testDatas = new TestData();
			Map<Integer, Cell> trainingDataCell 	= trainingData.getFactorCollection();
			ClassifyCaseControl classify 		= new ClassifyCaseControl();
			classify.Classify(testData, trainingData.getIndexSNP(), seed, false);
			Map<Integer, Cell> testDataCell 		= classify.getClassifyGroup();
			double TPfuzzy = 0, FNfuzzy = 0, FPfuzzy = 0, TNfuzzy = 0;
			
			for(Entry<Integer, Cell> test_cell:testDataCell.entrySet())
			{
				if(trainingDataCell.containsKey(test_cell.getKey()))
				{
					double wi1 = 0;
					double ni0 = (double)test_cell.getValue().getTotalControl()/test_cell.getValue().getWholeDataSize();
					double ni1 = (double)test_cell.getValue().getTotalCase()/test_cell.getValue().getWholeDataSize();
					double bni0 = ni0 / (ni0 + ni1);
					double bni1 = 1-bni0;
					
					if(test_cell.getValue().getTotalCase()==0) {
						wi1 = 0;
					}else if(test_cell.getValue().getTotalControl()==0) {
						wi1 = 1;
					}else {
						wi1 = 1/(1+Math.pow(Math.log(bni1)/Math.log(bni0), 2/(m-1)));
					}
					double wi2 = 1- wi1;
					TPfuzzy += (double)test_cell.getValue().getTotalCase() * wi1;
					FPfuzzy += (double)test_cell.getValue().getTotalControl() * wi1;
					FNfuzzy += (double)test_cell.getValue().getTotalCase() * wi2;
					TNfuzzy += (double)test_cell.getValue().getTotalControl() * wi2;
				}
			}
			
			testDatas.setTP(TPfuzzy);
			testDatas.setFP(FPfuzzy);
			testDatas.setTN(TNfuzzy);
			testDatas.setFN(FNfuzzy);
			
			ObjectiveSolution estimation = new ObjectiveSolution();
			for(fitnessFunction functions:MultifactorDimensionalityReduction.functions)
			{
				estimation.setFitnessFunction(functions, functions.getResults(TPfuzzy, FPfuzzy, FNfuzzy, TNfuzzy));
			}
			testDatas.setMultiObjectiveSolution(estimation);
			trainingDataCandidate.setParetoTestingData(testDatas);
		}
	}
}

/*
 * <p>
 * Copyright &copy; 2018-2019
 * Author: Yu-Da Lin
 * Web: https://www.researchgate.net/profile/Yu-Da_Lin
 * </p>
 * <p>
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * </p>
 * <p>
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more
 * details.
 * </p>
 * <p>
 * You should have received a copy of the GNU General Public
 * License along with this program.
 * </p>
 */
