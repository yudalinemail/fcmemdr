package Math;

public class CIvalue 
{
	double upper = 0.0d;
	double lower = 0.0d;
	double oddsRatio = 0.0d;
	
	public CIvalue(double oddsratio, double upper, double lower)
	{
		setOddsRatio(oddsratio);
		setUpper(upper);
		setLower(lower);
	}
	
	public void setOddsRatio(double oddsratio)
	{
		this.oddsRatio = oddsratio;
	}
	
	public void setUpper(double upper)
	{
		this.upper = upper;
	}
	
	public void setLower(double lower)
	{
		this.lower = lower;
	}
	
	public double getOddsRatio()
	{
		return oddsRatio;
	}
	
	public double getUpper()
	{
		return upper;
	}
	
	public double getLower()
	{
		return lower;
	}
	
}

/*
 * <p>
 * Copyright &copy; 2018-2019
 * Author: Yu-Da Lin
 * Web: https://www.researchgate.net/profile/Yu-Da_Lin
 * </p>
 * <p>
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * </p>
 * <p>
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more
 * details.
 * </p>
 * <p>
 * You should have received a copy of the GNU General Public
 * License along with this program.
 * </p>
 */
