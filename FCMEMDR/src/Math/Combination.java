package Math;

import java.math.BigDecimal;

public class Combination 
{
	public Combination()
	{
		
	}
	
	public BigDecimal C(int n, int k)
	{
		BigDecimal a = new BigDecimal("1");
		for(int i = 1 ; i <= k ; i++)
		{
			a = a.multiply(new BigDecimal(Integer.toString(i)));
		}
		BigDecimal b = new BigDecimal("1");
		for(int i = n - k + 1 ; i <= n ; i++)
		{
			b = b.multiply(new BigDecimal(Integer.toString(i)));
		}
		return b.divide(a);
	}
	
}

/*
 * <p>
 * Copyright &copy; 2018-2019
 * Author: Yu-Da Lin
 * Web: https://www.researchgate.net/profile/Yu-Da_Lin
 * </p>
 * <p>
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * </p>
 * <p>
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more
 * details.
 * </p>
 * <p>
 * You should have received a copy of the GNU General Public
 * License along with this program.
 * </p>
 */
