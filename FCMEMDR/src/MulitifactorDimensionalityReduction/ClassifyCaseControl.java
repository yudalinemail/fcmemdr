package MulitifactorDimensionalityReduction;

import java.util.HashMap;
import java.util.Map;


public class ClassifyCaseControl 
{
	private Map<Integer, Cell> m_ClassifyGroup = new HashMap<Integer, Cell>();
	
	public ClassifyCaseControl()
	{
		
	}
	
	public void Classify(Data data, int[] indexSNP, int seed, boolean trainingORtest)
	{
		//if trainingORtest = true, it represent the training
		//if trainingORtest = false, it represent the test
		for(int i=0;i<data.getDataSize();i++){
			boolean caseOrcontrol = data.getSampleType(i);
			int genotype = 0;
			int ss = 1;
			for(int j=0;j<indexSNP.length;j++){
				genotype += data.getData()[i].getIndexOfElements(indexSNP[j])*ss;
				ss*=10;
			}
			if(m_ClassifyGroup.containsKey(genotype))
			{
				if(caseOrcontrol)
				{
					m_ClassifyGroup.get(genotype).IncreaseCaseCount();
				}
				else
				{
					m_ClassifyGroup.get(genotype).IncreaseControlCount();
				}
			}			
			else
			{
				Cell group = new Cell(caseOrcontrol);				
				m_ClassifyGroup.put(genotype, group);
			}
		}
		
		for(Cell cell:m_ClassifyGroup.values()){
			cell.setWholeDataSize(data.getDataSize());
			cell.setWholeCaseSize(data.getCaseNumber());
			cell.setWholeControlSize(data.getControlNumber());
			cell.setCellValue
			(
				new CalculateCell().Calculate
				(
					cell, 	data.getCaseNumber(), 	data.getControlNumber()
				)
			);
		}
	}	
	
	public Map<Integer, Cell> getClassifyGroup()
	{
		return m_ClassifyGroup;
	}
	
}

/*
 * <p>
 * Copyright &copy; 2015-2019
 * Author: Yu-Da Lin
 * Web: https://www.researchgate.net/profile/Yu-Da_Lin
 * </p>
 * <p>
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * </p>
 * <p>
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more
 * details.
 * </p>
 * <p>
 * You should have received a copy of the GNU General Public
 * License along with this program.
 * </p>
 */
