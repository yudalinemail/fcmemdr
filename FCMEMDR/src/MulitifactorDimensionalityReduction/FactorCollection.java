package MulitifactorDimensionalityReduction;

import java.util.Map;

public class FactorCollection
{
	private Map<Integer, Cell> m_Combination = null;
	private ClassifyCaseControl m_Classify = null;
	
	public FactorCollection(Data data, int[] indexSNP, int seed, boolean trainingORtest)
	{
		ClassifyCaseControl(data, indexSNP, seed, trainingORtest);
	}
	
	private void ClassifyCaseControl(Data data, int[] indexSNP, int seed, boolean trainingORtest)
	{
		m_Classify = new ClassifyCaseControl();
		m_Classify.Classify(data, indexSNP, seed, trainingORtest);
		m_Combination = m_Classify.getClassifyGroup();
	}
	
	public Map<Integer, Cell> getFactorCollection()
	{
		return m_Combination;
	}
	
}

/*
 * <p>
 * Copyright &copy; 2015-2019
 * Author: Yu-Da Lin
 * Web: https://www.researchgate.net/profile/Yu-Da_Lin
 * </p>
 * <p>
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * </p>
 * <p>
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more
 * details.
 * </p>
 * <p>
 * You should have received a copy of the GNU General Public
 * License along with this program.
 * </p>
 */