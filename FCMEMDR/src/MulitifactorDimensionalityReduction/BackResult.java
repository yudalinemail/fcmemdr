package MulitifactorDimensionalityReduction;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BackResult implements Serializable
{
	
	private static final long serialVersionUID = 2558905031559983414L;
	private StringBuffer whole = new StringBuffer();
	private StringBuffer fold = new StringBuffer();
	private String functionName = "";
	
	public void appendWhole(String result)
	{
		whole.append(result);
	}
	
	public void appendFold(String result)
	{
		fold.append(result);
	}
	
	public StringBuffer getWhole()
	{
		return whole;
	}
	
	public StringBuffer getFold()
	{
		return fold;
	}
	
	public void toStringFold()
	{
		System.out.println(fold.toString());
	}
	
	public void setFunctionName(String name)
	{
		functionName = name;
	}
	
	public String getFunctionName()
	{
		return functionName;
	}
	
	public void toSaveData(String fileName)
	{
		saveFold(checkAndCreateDirectory(functionName, fileName), fileName);
		saveWhole(checkAndCreateDirectory(functionName, fileName), fileName);
	}
	
	public ArrayList<String> getBestSolutions()
	{
		Map<Integer, ArrayList<String>> bestSolutions = new HashMap<Integer, ArrayList<String>>();
		String[] lines = whole.toString().split("\n");
		int maxNumber = 0;
		for(String contents:lines)
		{
			if(contents.contains("\t")!=true) continue;
			String[] content = contents.split("\t");
			int number = Integer.valueOf(content[1]);
			maxNumber = number>maxNumber ? number:maxNumber;
			if(bestSolutions.containsKey(number))
			{
				bestSolutions.get(number).add(contents);
			}else
			{
				ArrayList<String> cont = new ArrayList<String>();
				cont.add(contents);
				bestSolutions.put(number, cont);
			}
		}
		
		return bestSolutions.get(maxNumber);
	}
	
	private File checkAndCreateDirectory(String modelName, String fileName)
	{
		File fileDir = new File( System.getProperty( "user.dir" ) + "\\Results\\" + modelName );
		if(!fileDir.isDirectory())
		{
			fileDir.mkdirs();
		}
		return fileDir;
	}
	
	private void saveFold(File file, String fileName)
	{
		System.out.println("Save to " + file.getAbsolutePath());
		try {
			File foldFile = new File(file, fileName);
			BufferedWriter output = new BufferedWriter(new FileWriter(foldFile.getAbsolutePath(),false));
			output.write(fold.toString());
			output.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void saveWhole(File file, String fileName)
	{
		try {
			File foldFile = new File(file, "Summary.txt");
			BufferedWriter output = new BufferedWriter(new FileWriter(foldFile.getAbsolutePath(),true));
			output.write("---------------" + fileName + "---------------\n");
			output.write(whole.toString());
			output.newLine();
			output.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}

/*
 * <p>
 * Copyright &copy; 2018-2019
 * Author: Yu-Da Lin
 * Web: https://www.researchgate.net/profile/Yu-Da_Lin
 * </p>
 * <p>
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * </p>
 * <p>
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more
 * details.
 * </p>
 * <p>
 * You should have received a copy of the GNU General Public
 * License along with this program.
 * </p>
 */
