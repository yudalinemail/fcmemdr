package MulitifactorDimensionalityReduction;

import java.io.File;
import java.util.Map;

import DataProcess.Preprocess;
import FitnessFunction.AdjCorrectClassificationFunction;
import FitnessFunction.LikelihoodRatioFunction;
import FitnessFunction.fitnessFunction;
import Solution.DefineObjective;

public class MultifactorDimensionalityReduction 
{
	
	public static fitnessFunction[] functions = null;
	public static Map<Integer, Boolean> objective = null;
	
	public MultifactorDimensionalityReduction()
	{
		
	}
	
	public BackResult implement(File file, String[] fitnessFunctions, int seed, String functionNames, int numberOfOrder, int numberOfCV)
	{
		assignFitnessFunctions(fitnessFunctions);
		objective = new DefineObjective(functions).getObjective();		
		
		Preprocess dataPreprocess = new Preprocess(file , seed);				
							
		final long StartTime = System.currentTimeMillis();	
		final CrossValidation cv = new CrossValidation(dataPreprocess.getNfoldData().getnfoldDataArrayList(), numberOfCV, numberOfOrder, seed);	
		long EndTime = System.currentTimeMillis();
		System.out.println("Implement Time:\t" + (double)(EndTime-StartTime)/1000);
		cv.getResult().PrintParetoSet();
		cv.getResult().determineBestSolutionsInParetoSet();
		cv.getResult().setFunctionName(functionNames);
		return cv.getResult().getResultString();
	}
	
	private void assignFitnessFunctions(String[] fitnessFunctions)
	{
		functions = new fitnessFunction[fitnessFunctions.length];
		int index = 0;
		for(String functionName:fitnessFunctions){
			switch(functionName){
				case "LR":
					functions[index++] = new LikelihoodRatioFunction();
					break;
				case "aCCR":
					functions[index++] = new AdjCorrectClassificationFunction();
					break;
			}
		}
	}
	
	public static void main(String[] s){
		new MultifactorDimensionalityReduction();
	}
}

/*
 * <p>
 * Copyright &copy; 2015-2019
 * Author: Yu-Da Lin
 * Web: https://www.researchgate.net/profile/Yu-Da_Lin
 * </p>
 * <p>
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * </p>
 * <p>
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more
 * details.
 * </p>
 * <p>
 * You should have received a copy of the GNU General Public
 * License along with this program.
 * </p>
 */
