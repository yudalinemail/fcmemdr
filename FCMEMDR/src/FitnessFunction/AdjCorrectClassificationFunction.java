package FitnessFunction;

public class AdjCorrectClassificationFunction implements fitnessFunction {

	@Override
	public double getResults(double TP, double FP, double FN, double TN) {
		// TODO Auto-generated method stub
		return 0.5*( TP / ( FN + TP ) + TN / ( FP + TN ));
	}

	@Override
	public boolean getMinOrMax() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public String getFunctionName() {
		// TODO Auto-generated method stub
		return "CCR";
	}

	@Override
	public int getFunctionCode() {
		// TODO Auto-generated method stub
		return 1;
	}
	
}

/*
 * <p>
 * Copyright &copy; 2018-2019
 * Author: Yu-Da Lin
 * Web: https://www.researchgate.net/profile/Yu-Da_Lin
 * </p>
 * <p>
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * </p>
 * <p>
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more
 * details.
 * </p>
 * <p>
 * You should have received a copy of the GNU General Public
 * License along with this program.
 * </p>
 */
