package FitnessFunction;

import java.math.BigDecimal;

import Math.Combination;

public class PvalueFunction implements fitnessFunction {

	@Override
	public double getResults(double TP, double FP, double FN, double TN) {
		// TODO Auto-generated method stub
		double X =TP;
		double r1 = TP + FP;
		double r2 = FN + TN;
		double d1 = TP + FN;
		double t = TP + FP + FN + TN;
		BigDecimal p3 = new Combination().C((int)t, (int)d1);
		BigDecimal value = new BigDecimal("0");
		for(int i = (int)X ; i <= r1 ; i++) {
			BigDecimal p1 = new Combination().C((int)r1, i);
			BigDecimal p2 = new Combination().C((int)r2, (int)d1-i);
			BigDecimal pp = p1.multiply(p2);
			pp = pp.divide(p3, 100, BigDecimal.ROUND_DOWN);
			value = value.add(pp);
		}
		
		return value.doubleValue();
	}

	@Override
	public boolean getMinOrMax() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getFunctionName() {
		// TODO Auto-generated method stub
		return "Pvalue";
	}

	@Override
	public int getFunctionCode() {
		// TODO Auto-generated method stub
		return 9;
	}

}

/*
 * <p>
 * Copyright &copy; 2018-2019
 * Author: Yu-Da Lin
 * Web: https://www.researchgate.net/profile/Yu-Da_Lin
 * </p>
 * <p>
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * </p>
 * <p>
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more
 * details.
 * </p>
 * <p>
 * You should have received a copy of the GNU General Public
 * License along with this program.
 * </p>
 */
