package FitnessFunction;

public interface fitnessFunction 
{
	public double getResults(double TP, double FP, double FN, double TN);
	
	/**
	 * get fitness function belongs to maximum or minimum.
	 * false: minimum
	 * true: maximum 
	 * */
	public boolean getMinOrMax();
	
	public String getFunctionName();
	
	public int getFunctionCode();
}

/*
 * <p>
 * Copyright &copy; 2018-2019
 * Author: Yu-Da Lin
 * Web: https://www.researchgate.net/profile/Yu-Da_Lin
 * </p>
 * <p>
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * </p>
 * <p>
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE. See the GNU General Public License for more
 * details.
 * </p>
 * <p>
 * You should have received a copy of the GNU General Public
 * License along with this program.
 * </p>
 */